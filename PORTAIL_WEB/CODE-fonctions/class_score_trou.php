<?php

	class Score_Trou
	{
		// ATTRIBUTS
		
		public $numero; // numéro du trou
		public $scoregroup; // groupe du score --> permet de différencier les enregistrements pour un même club à une même date
		public $score; // score pour ce trou
		public $distance;
		public $par;
		

		// ACCESSEURS
		
		public function get_numero(){return $this->numero;}
		public function set_numero($unnumero){$this->numero = $unnumero;}
		
		public function get_scoregroup(){return $this->scoregroup;}
		public function set_scoregroup($unscoregroup){$this->scoregroup = $unscoregroup;}
		
		public function get_score(){return $this->score;}
		public function set_score($unscore){$this->score = $unscore;}
		
		public function get_distance(){return $this->distance;}
		public function set_distance($undistance){$this->distance = $undistance;}
		
		public function get_par(){return $this->par;}
		public function set_par($unpar){$this->par = $unpar;}
		
		// CONSTRUCTEUR
		
		public function __construct($unnumero, $unscoregroup, $unscore, $undistance, $unpar)
		{
			$this->numero = $unnumero;
			$this->scoregroup = $unscoregroup;
			$this->score = $unscore;
			$this->distance = $undistance;
			$this->par = $unpar;
		}
	}
	
?>