<?php

	class Score_Club
	{
		// ATTRIBUTS
		
		public $code; // code du club
		public $nom; // nom du club
		public $ville; // ville du club
		public $datesave; // date d'enregistrement du score
		public $distance_tot; // distance totale du parcours
		public $par_tot; // PAR total du parcours
		public $score_tot; // score total du parcours
		public $nom_joueur;
		
		public $les_scores_trous; // collection d'objet Score_Trou
		
		
		// ACCESSEURS
		
		public function get_code(){return $this->code;}
		public function set_code($uncode){$this->code = $uncode;}
		
		public function get_nom(){return $this->nom;}
		public function set_nom($unnom){$this->nom = $unnom;}
		
		public function get_ville(){return $this->ville;}
		public function set_ville($unville){$this->ville = $unville;}
		
		public function get_datesave(){return $this->datesave;}
		public function set_datesave($undatesave){$this->datesave = $undatesave;}
		
		public function get_distance_tot(){return $this->distance_tot;}
		public function set_distance_tot($undistance_tot){$this->distance_tot = $undistance_tot;}
		
		public function get_par_tot(){return $this->par_tot;}
		public function set_par_tot($unpar_tot){$this->par_tot = $unpar_tot;}
		
		public function get_score_tot(){return $this->score_tot;}
		public function set_score_tot($unscore_tot){$this->score_tot = $unscore_tot;}
		
		public function get_nom_joueur(){return $this->nom_joueur;}
		public function set_nom_joueur($unnom_joueur){$this->nom_joueur = $unnom_joueur;}
		
		public function get_les_scores_trous(){return $this->les_scores_trous;}
		
		
		// CONSTRUCTEUR
		
		public function __construct($uncode, $unnom, $unville/*, $undatesave, $undistance_tot, $unpar_tot, $unscore_tot*/)
		{
			$this->code = $uncode;
			$this->nom = $unnom;
			$this->ville = $unville;
			//$this->datesave = $undatesave;
			//$this->distance_tot = $undistance_tot;
			//$this->par_tot = $unpar_tot;
			//$this->score_tot = $unscore_tot;
			$this->les_scores_trous = array();
		}
	}
	
?>