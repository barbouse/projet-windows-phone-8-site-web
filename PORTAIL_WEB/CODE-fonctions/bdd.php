﻿<?php


class PdoProjetGMA
{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=projet_gma';   		
      	private static $user='root' ;    		
      	private static $mdp='' ;	
		private static $monPdo;
		private static $monPdoProjetGMA = null;

		
	private function __construct()
	{
    		PdoProjetGMA::$monPdo = new PDO(PdoProjetGMA::$serveur.';'.PdoProjetGMA::$bdd, PdoProjetGMA::$user, PdoProjetGMA::$mdp); 
			PdoProjetGMA::$monPdo->query("SET CHARACTER SET utf8");
	}
	
	public function _destruct(){
		PdoProjetGMA::$monPdo = null;
	}

	public  static function getPdoProjetGMA()
	{
		if(PdoProjetGMA::$monPdoProjetGMA == null)
		{
			PdoProjetGMA::$monPdoProjetGMA= new PdoProjetGMA();
		}
		return PdoProjetGMA::$monPdoProjetGMA;  
	}
	
	
	
	public function verifco($pseudo, $mdp)
    {
		$safepseudo = htmlspecialchars($pseudo);
		$safemdp = md5(htmlspecialchars($mdp));
	
		if ($safepseudo!="" && $safemdp!="")
		{	
			$check = PdoProjetGMA::$monPdo->query('SELECT COUNT(*) FROM joueurs WHERE login="'.$safepseudo.'" AND mdp="'.$safemdp.'"')->fetchcolumn();
	
			if($check >= 1)
			{		
				session_unset();
				$_SESSION["pseudo"] = $safepseudo;
			}
		}
    }
	
	
	
	public function deconnect()
	{
		session_unset();
	}
	
	
	
	public function inscription($pseudo, $mdp)
	{
		$pseudo = htmlspecialchars($pseudo);
		$mdp = md5($mdp);
		
	
		if ($pseudo!="" && $mdp!="")
		{				
			// On récupère le nombre de pseudos de la table users et on fetch la colonne renvoyée
			$check = PdoProjetGMA::$monPdo->query('SELECT COUNT(*) FROM joueurs WHERE login="'.$pseudo.'"')->fetchColumn();
		
			if($check >= 1)	
				return(1);
			else
			{
				$req = "INSERT INTO joueurs VALUES ('".$pseudo."', '".$mdp."')";
				$res = PdoProjetGMA::$monPdo->query($req);
				return(3);
			}
				
		}
		else
			return(2);
	}
	
	
	public function historique()
	{
		// Le tableau final qui contiendra tous les scores du joueur
		// $_SESSION["scores_joueur"] est donc un tableau d'objet Score_Club
		
			unset($_SESSION["scores_joueur"]);
			$_SESSION["scores_joueur"] = array();
		
		
			$lesscoregroups = PdoProjetGMA::$monPdo->query("SELECT scoregroup, clubs.code, nom, ville 
															FROM scoretrous 
															INNER JOIN trous
															ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
															INNER JOIN clubs
															ON trous.code = clubs.code
															WHERE login='".$_SESSION["pseudo"]."' 
															GROUP BY scoregroup
															ORDER BY datesave DESC");
			$lesscoregroups->setFetchMode(PDO::FETCH_OBJ);
			
			
		// Pour chaque scoregroup du joueur ...
		
			while( $ligne = $lesscoregroups->fetch() )
			{
				$code = $ligne->code;
				$nom = $ligne->nom;
				$ville = $ligne->ville;
				
				// Création d'un objet Score_Club dans lequel on met $tabtrous
				$club = new Score_Club($code, $nom, $ville);

					
				$distance_tot = 0;
				$par_tot = 0;
				$score_tot = 0;
				
				
				$les_scores_trous = PdoProjetGMA::$monPdo->query("SELECT scoretrous.datesave, trous.numero, score, distance, par 
																	FROM scoretrous 
																	INNER JOIN trous
																	ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
																	INNER JOIN clubs
																	ON trous.code = clubs.code
																	WHERE scoregroup='".$ligne->scoregroup."'
																	ORDER BY numero DESC");
				$les_scores_trous->setFetchMode(PDO::FETCH_OBJ);
				
				
				// ... on prend les score_trou correspondant et on les stocke dans un tableau
				
					while( $ligne2 = $les_scores_trous->fetch() )
					{
						
						$datesave = $ligne2->datesave;
						$numero = $ligne2->numero;
						$score = $ligne2->score;
						$distance = $ligne2->distance;
						$par = $ligne2->par;
						
						$distance_tot += $distance;
						$par_tot += $par;
						$score_tot += $score;
						
						
						// Création d'un objet Score_Trou
							$trou = new Score_Trou($ligne2->numero, $ligne->scoregroup, $ligne2->score, $ligne2->distance, $ligne2->par);
							
						// Rangement de cet objet dans le tableau $tabtrous
							$club->les_scores_trous[] = $trou;
					}
				
					$club->datesave = $datesave;
					$club->distance_tot = $distance_tot;
					$club->par_tot = $par_tot;
					$club->score_tot = $score_tot;

				
				
				// Rangement de $club dans $_SESSION["scores_joueur"] qui sera exploitée directement sur la page historique
					$_SESSION["scores_joueur"][] = $club;

			}
	}
	
	
	
	public function classement_club($code_nom)
	{
		$saisie = htmlspecialchars($code_nom);
		
					
		// On regarde si ce code de club existe dans la BDD (dans le cas hypothétique où c'est un code)
		$check_code = PdoProjetGMA::$monPdo->query('SELECT COUNT(*) FROM clubs WHERE code="'.$saisie.'"')->fetchColumn();
		
		// On regarde si ce nom de club existe dans la BDD (dans le cas hypothétique où c'est un nom)
		$check_nom = PdoProjetGMA::$monPdo->query('SELECT COUNT(*) FROM clubs WHERE nom="'.$saisie.'"')->fetchColumn();
	
		if($check_code >= 1 || $check_nom >= 1)	
		{
			if($check_code >= 1)
				$type = "code";
			else
				$type = "nom";
				

				
		// Le tableau final qui contiendra les scores de ce club classés par ordre ascendant
		// $_SESSION["classement"] est donc un tableau d'objet Score_Club
		
			unset($_SESSION["classement"]);
			$_SESSION["classement"] = array();
			
			
			$lesscoregroups = PdoProjetGMA::$monPdo->query("SELECT scoregroup, clubs.code, nom, ville 
															FROM scoretrous 
															INNER JOIN trous
															ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
															INNER JOIN clubs
															ON trous.code = clubs.code
															WHERE clubs.".$type."='".$saisie."' 
															GROUP BY scoregroup");
			$lesscoregroups->setFetchMode(PDO::FETCH_OBJ);
			
			
		// Pour chaque scoregroup ...
		
			while( $ligne = $lesscoregroups->fetch() )
			{
				unset($_SESSION["code_club"]);
				$_SESSION["code_club"] = $ligne->code;
				
				$code = $ligne->code;
				$nom = $ligne->nom;
				$ville = $ligne->ville;
				
				// Création d'un objet Score_Club dans lequel on met $tabtrous
				$club = new Score_Club($code, $nom, $ville);

					
				$distance_tot = 0;
				$par_tot = 0;
				$score_tot = 0;
				
				
				$les_scores_trous = PdoProjetGMA::$monPdo->query("SELECT login, scoretrous.datesave, trous.numero, score, distance, par 
																	FROM scoretrous 
																	INNER JOIN trous
																	ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
																	INNER JOIN clubs
																	ON trous.code = clubs.code
																	WHERE scoregroup='".$ligne->scoregroup."'
																	ORDER BY numero DESC");
				$les_scores_trous->setFetchMode(PDO::FETCH_OBJ);
				
				
				// ... on prend les score_trou correspondant et on les stocke dans un tableau
				
					while( $ligne2 = $les_scores_trous->fetch() )
					{
						$login = $ligne2->login;
						$datesave = $ligne2->datesave;
						$numero = $ligne2->numero;
						$score = $ligne2->score;
						$distance = $ligne2->distance;
						$par = $ligne2->par;
						
						$distance_tot += $distance;
						$par_tot += $par;
						$score_tot += $score;
						
						
						// Création d'un objet Score_Trou
							$trou = new Score_Trou($ligne2->numero, $ligne->scoregroup, $ligne2->score, $ligne2->distance, $ligne2->par);
							
						// Rangement de cet objet dans le tableau $tabtrous
							$club->les_scores_trous[] = $trou;
					}
				
					$club->datesave = $datesave;
					$club->distance_tot = $distance_tot;
					$club->par_tot = $par_tot;
					$club->score_tot = $score_tot;
					$club->nom_joueur = $login;
					
				
				// Rangement de $club dans $_SESSION["classement"] qui sera exploitée directement sur la page classement
					$_SESSION["classement"][] = $club;
			}
			
			// On tri $_SESSION["classement"] selon score_tot ASC
				for($i=0 ; $i<sizeof($_SESSION["classement"]) ; $i++)
				{
					for($j=0 ; $j<sizeof($_SESSION["classement"]) ; $j++)
					{
						if($_SESSION["classement"][$i]->score_tot < $_SESSION["classement"][$j]->score_tot)
						{
							$tempo = $_SESSION["classement"][$j];
							$_SESSION["classement"][$j] = $_SESSION["classement"][$i];
							$_SESSION["classement"][$i] = $tempo;
						}
					}
				}
				
			// On supprime les doublons de joueur en gardant le score le plus faible pour chacun d'entre eux
				for($i=0 ; $i<sizeof($_SESSION["classement"]) ; $i++)
				{
					for($j=0 ; $j<sizeof($_SESSION["classement"]) ; $j++)
					{
						$encours_i = $_SESSION["classement"][$i];
						$encours_j = $_SESSION["classement"][$j];
						
						if($encours_i != null && $encours_j != null)
						{
							if(($encours_i->nom_joueur === $encours_j->nom_joueur) && ($i != $j))
							{
								if($_SESSION["classement"][$i]->score_tot < $_SESSION["classement"][$j]->score_tot)
									$_SESSION["classement"][$j] = null;
								else
									$_SESSION["classement"][$i] = null;
							}
						}
					}
				}
			
			
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	
	
	public function classement_trou($type_classement)
	{
		
		if($type_classement === "Global")
		{
			// On ne fait rien, on retourne au constructeur qui redirige vers la vue qui affichera le contenu de $_SESSION["classement"]
			// $_SESSION["classement"] correspond en fait au classement Global
			
			return "Global";
		}
		else
		{
			
			// Le tableau final qui contiendra les scores de ce club classés par score du trou choisi
			// $_SESSION["classement_selon_trou"] est donc un tableau d'objet Score_Club
			
				unset($_SESSION["classement_selon_trou"]);
				$_SESSION["classement_selon_trou"] = array();
				
				
				$lesscoregroups = PdoProjetGMA::$monPdo->query("SELECT scoregroup, clubs.code, nom, ville 
																FROM scoretrous 
																INNER JOIN trous
																ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
																INNER JOIN clubs
																ON trous.code = clubs.code
																WHERE clubs.code='".$_SESSION["code_club"]."' 
																GROUP BY scoregroup");
				$lesscoregroups->setFetchMode(PDO::FETCH_OBJ);
				
				
			// Pour chaque scoregroup ...
			
				while( $ligne = $lesscoregroups->fetch() )
				{
					$code = $ligne->code;
					$nom = $ligne->nom;
					$ville = $ligne->ville;
					
					// Création d'un objet Score_Club dans lequel on met $tabtrous
					$club = new Score_Club($code, $nom, $ville);

						
					$distance_tot = 0;
					$par_tot = 0;
					$score_tot = 0;
					
					
					$les_scores_trous = PdoProjetGMA::$monPdo->query("SELECT login, scoretrous.datesave, trous.numero, score, distance, par 
																		FROM scoretrous 
																		INNER JOIN trous
																		ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
																		INNER JOIN clubs
																		ON trous.code = clubs.code
																		WHERE scoregroup='".$ligne->scoregroup."'
																		ORDER BY numero DESC");
					$les_scores_trous->setFetchMode(PDO::FETCH_OBJ);
					
					
					// ... on prend les score_trou correspondant et on les stocke dans un tableau
					
						while( $ligne2 = $les_scores_trous->fetch() )
						{
							$login = $ligne2->login;
							$datesave = $ligne2->datesave;
							$numero = $ligne2->numero;
							$score = $ligne2->score;
							$distance = $ligne2->distance;
							$par = $ligne2->par;
							
							$distance_tot += $distance;
							$par_tot += $par;
							$score_tot += $score;
							
							
							// Création d'un objet Score_Trou
								$trou = new Score_Trou($ligne2->numero, $ligne->scoregroup, $ligne2->score, $ligne2->distance, $ligne2->par);
								
							// Rangement de cet objet dans le tableau $tabtrous
								$club->les_scores_trous[] = $trou;
						}
					
						$club->datesave = $datesave;
						$club->distance_tot = $distance_tot;
						$club->par_tot = $par_tot;
						$club->score_tot = $score_tot;
						$club->nom_joueur = $login;
						
					
					// Rangement de $club dans $_SESSION["classement"] qui sera exploitée directement sur la page classement
						$_SESSION["classement_selon_trou"][] = $club;
				}
				
			
				// On tri $_SESSION["classement_selon_trou"] selon le trou choisi
					for($i=0 ; $i<sizeof($_SESSION["classement_selon_trou"]) ; $i++)
					{
						// Récupération du trou choisi
						$indice = 0;
						foreach($_SESSION["classement_selon_trou"][$i]->les_scores_trous as $trou)
						{
							if($trou->numero == $type_classement)
								$indice_trou_i = $indice;
							$indice++;
						}
						
						
						for($j=0 ; $j<sizeof($_SESSION["classement_selon_trou"]) ; $j++)
						{
							// Récupération du trou choisi
							$indice = 0;
							foreach($_SESSION["classement_selon_trou"][$j]->les_scores_trous as $trou)
							{
								if($trou->numero == $type_classement)
									$indice_trou_j = $indice;
								$indice++;
							}
							
							$score_i = $_SESSION["classement_selon_trou"][$i]->les_scores_trous[$indice_trou_i]->score;
							$score_j = $_SESSION["classement_selon_trou"][$j]->les_scores_trous[$indice_trou_j]->score;
							
							if($score_i < $score_j)
							{
								$tempo = $_SESSION["classement_selon_trou"][$j];
								$_SESSION["classement_selon_trou"][$j] = $_SESSION["classement_selon_trou"][$i];
								$_SESSION["classement_selon_trou"][$i] = $tempo;
							}
						}
					}
					
				// On supprime les doublons de joueur en gardant le score le plus faible pour chacun d'entre eux
					for($i=0 ; $i<sizeof($_SESSION["classement_selon_trou"]) ; $i++)
					{
						for($j=0 ; $j<sizeof($_SESSION["classement_selon_trou"]) ; $j++)
						{
							$encours_i = $_SESSION["classement_selon_trou"][$i];
							$encours_j = $_SESSION["classement_selon_trou"][$j];
							
							if($encours_i != null && $encours_j != null)
							{
								if(($encours_i->nom_joueur === $encours_j->nom_joueur) && ($i != $j))
								{
									$score_i = $_SESSION["classement_selon_trou"][$i]->les_scores_trous[$indice_trou_i]->score;
									$score_j = $_SESSION["classement_selon_trou"][$j]->les_scores_trous[$indice_trou_j]->score;
									
									if($score_i < $score_j)
										$_SESSION["classement_selon_trou"][$j] = null;
									else
										$_SESSION["classement_selon_trou"][$i] = null;
								}
							}
						}
					}
			return "Trou";
		}	
	}
	
	
	
	public function historique_specifique($code_nom)
	{
		$saisie = htmlspecialchars($code_nom);
					
		// On regarde si ce code de club existe dans la BDD (dans le cas hypothétique où c'est un code)
		$check_code = PdoProjetGMA::$monPdo->query('SELECT COUNT(*) FROM clubs WHERE code="'.$saisie.'"')->fetchColumn();
		
		// On regarde si ce nom de club existe dans la BDD (dans le cas hypothétique où c'est un nom)
		$check_nom = PdoProjetGMA::$monPdo->query('SELECT COUNT(*) FROM clubs WHERE nom="'.$saisie.'"')->fetchColumn();
	
		if($check_code >= 1 || $check_nom >= 1)	
		{
			if($check_code >= 1)
				$type = "code";
			else
				$type = "nom";
				

				
		// Le tableau final qui contiendra les scores de ce club classés par ordre ascendant
		// $_SESSION["histo_spec"] est donc un tableau d'objet Score_Club
		
			unset($_SESSION["histo_spec"]);
			$_SESSION["histo_spec"] = array();
			
			
			$lesscoregroups = PdoProjetGMA::$monPdo->query("SELECT scoregroup, clubs.code, nom, ville 
															FROM scoretrous 
															INNER JOIN trous
															ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
															INNER JOIN clubs
															ON trous.code = clubs.code
															WHERE clubs.".$type."='".$saisie."'
															AND login='".$_SESSION["pseudo"]."'
															GROUP BY scoregroup
															ORDER BY datesave DESC");
			$lesscoregroups->setFetchMode(PDO::FETCH_OBJ);
			
			
		// Pour chaque scoregroup ...
		
			while( $ligne = $lesscoregroups->fetch() )
			{
				$code = $ligne->code;
				$nom = $ligne->nom;
				$ville = $ligne->ville;
				
				// Création d'un objet Score_Club dans lequel on met $tabtrous
				$club = new Score_Club($code, $nom, $ville);

					
				$distance_tot = 0;
				$par_tot = 0;
				$score_tot = 0;
				
				
				$les_scores_trous = PdoProjetGMA::$monPdo->query("SELECT login, scoretrous.datesave, trous.numero, score, distance, par 
																	FROM scoretrous 
																	INNER JOIN trous
																	ON scoretrous.code = trous.code AND scoretrous.numero = trous.numero
																	INNER JOIN clubs
																	ON trous.code = clubs.code
																	WHERE scoregroup='".$ligne->scoregroup."'
																	ORDER BY numero DESC");
				$les_scores_trous->setFetchMode(PDO::FETCH_OBJ);
				
				
				// ... on prend les score_trou correspondant et on les stocke dans un tableau
				
					while( $ligne2 = $les_scores_trous->fetch() )
					{
						$login = $ligne2->login;
						$datesave = $ligne2->datesave;
						$numero = $ligne2->numero;
						$score = $ligne2->score;
						$distance = $ligne2->distance;
						$par = $ligne2->par;
						
						$distance_tot += $distance;
						$par_tot += $par;
						$score_tot += $score;
						
						
						// Création d'un objet Score_Trou
							$trou = new Score_Trou($ligne2->numero, $ligne->scoregroup, $ligne2->score, $ligne2->distance, $ligne2->par);
							
						// Rangement de cet objet dans le tableau $tabtrous
							$club->les_scores_trous[] = $trou;
					}
				
					$club->datesave = $datesave;
					$club->distance_tot = $distance_tot;
					$club->par_tot = $par_tot;
					$club->score_tot = $score_tot;
					$club->nom_joueur = $login;
					
				
				// Rangement de $club dans $_SESSION["histo_spec"] qui sera exploitée directement sur la page historique
					$_SESSION["histo_spec"][] = $club;
			}
			
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	

}

?>


















