
<h3>Classement</h3>

	<br />
		  
	<form id="myform" class="form-horizontal" action="index.php?cas=classement&action=chercher" method="post">

		  <div class="form-group row">
			<label for="chercher_club" class="col-sm-2 col-sm-offset-0 control-label">Nom/Code Club</label>
			<div class="col-sm-2 col-sm-offset-0">
			  <input type="text" name="chercher_club" class="form-control required formborder" id="chercher_club" <?php if(isset($_REQUEST["chercher_club"])){echo 'value="'.$_REQUEST["chercher_club"].'"';} ?> placeholder="Nom/Code Club" required/>
			</div>
		  </div>
		  
		  </br>
		  
		  <div class="form-group row">
			<div class="col-sm-offset-2 col-sm-2">
			  <input type="submit" class="btn btn-default" onClick="" value="Afficher" />
			</div>
		  </div>
	
	</form>
	  
	</br>
	



<?php

	 //var_dump($_SESSION["classement"][0]->les_scores_trous);
	 //var_dump($_SESSION["classement"][1]->les_scores_trous);

	if(isset($_SESSION["classement"]) || isset($_SESSION["classement_selon_trou"]))
	{
		if(isset($_SESSION["classement"]))
			$TAB_A_AFFICHER = $_SESSION["classement"];
		if(isset($_SESSION["classement_selon_trou"]))
			$TAB_A_AFFICHER = $_SESSION["classement_selon_trou"];
		
		
		
		
		$check = 0;
		$i = 0;
		
		while($check != 1)
		{
			if($TAB_A_AFFICHER[$i] != null)
			{
				$check = 1;
				$ref = $TAB_A_AFFICHER[$i];
			}
			$i++;
		}
		
		
		if(isset($ref))
		{
		
			echo '<h2>'.($ref->nom).' - '.($ref->ville).'</h2>';
			echo "<h2>Distance Totale : ".($ref->distance_tot)."m</h2>";
			echo "<h2>PAR Total : " .($ref->par_tot)."</h2><br />";
					
?>
				<br />
				
				<form id="myform" class="form-horizontal" action="index.php?cas=classement&action=classement_trou" method="post">
				
						<div class="col-sm-offset-2 col-sm-10">
							<label for="type_classement" class="col-sm-2 col-sm-offset-0 control-label">Type Classement</label>
							<div class="btn-group" role="group" aria-label="...">
							
							
								<input type="submit" name="type_classement" id="type_classement" class="btn btn-default" onClick="" value="Global" />
							  
	<?php
								foreach($TAB_A_AFFICHER[$i-1]->les_scores_trous as $tab)
								{
									//echo '<button type="button" class="btn btn-default">Trou '.$tab->numero.'</button>';
									echo '<input type="submit" name="type_classement" class="btn btn-default" onClick="" value="'.$tab->numero.'" />';
								}
	?>				
							
							</div>
						</div>
				
				
				</form>
	
				<br /><br />
<?php

			if(!isset($_SESSION["num_trou"]) || $_SESSION["num_trou"] === "Global")
				echo '<h2>Classement Global</h2><br />';
			else
				echo '<h2>Classement Selon Score Trou '.$_SESSION["num_trou"].'</h2><br />';
				
				
				
				
			$i=0;
			
			foreach($TAB_A_AFFICHER as $tab)
			{
			
				if($tab != null)
				{
					$i++;
					
					// On récupère le score
					foreach($tab->les_scores_trous as $trou)
					{
						if(isset($_SESSION["num_trou"]))
						{
							if($trou->numero == $_SESSION["num_trou"])
							{
								unset($_SESSION["score_a_afficher"]);
								$_SESSION["score_a_afficher"] = $trou->score;
							}
						}
					}
?>
					<div class="list-group">

						<a href="#" class="list-group-item active" data-toggle="modal" data-target="#myModal<?php echo $i;?>">
							<h4 class="list-group-item-heading"><?php echo "Rang ".$i." - Joueur : ".($tab->nom_joueur); ?><?php if(isset($_SESSION["classement_selon_trou"])){echo '<br />Score Trou '.$_SESSION["num_trou"].' : '.$_SESSION["score_a_afficher"];}else{echo '<br />Score Total : '.($tab->score_tot).'<br />';} ?></h4>
							<p class="list-group-item-text">
								
							</p>
						</a>
					  
					  
						<!-- Modal -->
						<div class="modal fade" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Score Détaillé <?php echo ( "<br />Rang ".$i." - Joueur : ".($tab->nom_joueur)."<br />".($tab->nom)." - ".($tab->datesave) ); ?></h4>
							  </div>
							  <div class="modal-body">
<?php
									echo '<table class="table" border=2>';
									echo 	'<tr>
												<td><h6>TROU</h6></td><td><h6>DISTANCE</h6></td><td><h6>PAR</h6></td><td><h6>SCORE</h6></td>
											</tr>';
										
									foreach($tab->les_scores_trous as $letrou)
									{
										echo 	'<tr>
													<td><h6>'.$letrou->numero.'</h6></td>
													<td><h6>'.$letrou->distance.'</h6></td>
													<td><h6>'.$letrou->par.'</h6></td>
													<td><h6>'.$letrou->score.'</h6></td>';
													
											echo '</tr>';
									}
									echo '</table>';
									
									//echo '<td><ul class="nav nav-pills"><li><a href="index.php?cas=user&action=inscription_dans_assoc">PILLS BUTTON</a></li></ul></td>';
?>								
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
							  </div>
							</div>
						  </div>
						</div>	

					</div>
<?php	
				}
			}
		}
		
		unset($_SESSION["classement"]);
		unset($_SESSION["classement_selon_trou"]);
		unset($_SESSION["num_trou"]);
		unset($_SESSION["type_classement"]);
	}
	
?>

</div> 




