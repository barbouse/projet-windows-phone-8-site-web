
<h3>Historique</h3>
	
<br /><br />
		  
	<form id="myform" class="form-horizontal" action="index.php?cas=historique&action=chercher" method="post">

		  <div class="form-group row">
			<label for="chercher_club" class="col-sm-2 col-sm-offset-0 control-label">Nom/Code Club</label>
			<div class="col-sm-2 col-sm-offset-0">
			  <input type="text" name="chercher_club" class="form-control required formborder" id="chercher_club" <?php if(isset($_REQUEST["chercher_club"])){echo 'value="'.$_REQUEST["chercher_club"].'"';} ?> placeholder="Nom/Code Club" required/>
			</div>
		  </div>
		  
		  </br>
		  
		  <div class="form-group row">
			<div class="col-sm-offset-2 col-sm-2">
			  <input type="submit" class="btn btn-default" onClick="" value="Afficher" />
			</div>
		  </div>
	
	</form>
	  
	</br>

<?php

	if(isset($_SESSION["histo_spec"]))
	{
	
		echo "<h2>Progression - ".($_SESSION["histo_spec"][0]->nom)."</h2><br /><br />";

		$i=0;
		
		foreach($_SESSION["histo_spec"] as $tab)
		{
			$i++;
?>
			<div class="list-group">

				<a href="#" class="list-group-item active" data-toggle="modal" data-target="#myModal<?php echo $i;?>">
					<h4 class="list-group-item-heading"><?php echo $tab->nom.' - '.$tab->ville.' - '.$tab->datesave; ?></h4>
					<p class="list-group-item-text center">
						<?php echo 'Distance Totale : '.$tab->distance_tot.'m<br />
									PAR Total : '.$tab->par_tot.'<br />
									Votre Score Total : '.$tab->score_tot.'<br />';
						?>
					</p>
				</a>
			  
			  
				<!-- Modal -->
				<div class="modal fade" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Score Détaillé - <?php echo $tab->nom; ?> - <?php echo $tab->datesave; ?></h4>
					  </div>
					  <div class="modal-body">
<?php
							echo '<table class="table" border=2>';
							echo 	'<tr>
										<td><h6>TROU</h6></td><td><h6>DISTANCE</h6></td><td><h6>PAR</h6></td><td><h6>SCORE</h6></td>
									</tr>';
								
							foreach($tab->les_scores_trous as $letrou)
							{
								echo 	'<tr>
											<td><h6>'.$letrou->numero.'</h6></td>
											<td><h6>'.$letrou->distance.'</h6></td>
											<td><h6>'.$letrou->par.'</h6></td>
											<td><h6>'.$letrou->score.'</h6></td>';
											
									echo '</tr>';
							}
							echo '</table>';
							
							echo '<ul class="nav nav-pills"><li><a href="index.php?cas=classement&action=chercher_depuis_histo&nom_code='.$tab->nom.'"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Classement Club</a></li></ul>';
?>								
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
					  </div>
					</div>
				  </div>
				</div>	

			</div>
			
<?php				
		}
		
		unset($_SESSION["histo_spec"]);
			
	}

?>



<br /><br />

<h2>Historique Général</h2>

<br /><br />

<?php

	 //var_dump($_SESSION["scores_joueur"][0]->les_scores_trous);
	 //var_dump($_SESSION["scores_joueur"][1]->les_scores_trous);

	if(isset($_SESSION["scores_joueur"]))
	{
		if(isset($_SESSION["scores_joueur"][0]))
		{
			$i=0;	
			foreach($_SESSION["scores_joueur"] as $tab)
			{
				$i++;
?>
				<div class="list-group">

					<a href="#" class="list-group-item active" data-toggle="modal" data-target="#myModal<?php echo $i;?>">
						<h4 class="list-group-item-heading"><?php echo $tab->nom.' - '.$tab->ville.' - '.$tab->datesave; ?></h4>
						<p class="list-group-item-text center">
							<?php echo 'Distance Totale : '.$tab->distance_tot.'m<br />
										PAR Total : '.$tab->par_tot.'<br />
										Votre Score Total : '.$tab->score_tot.'<br />';
							?>
						</p>
					</a>
				  
				  
					<!-- Modal -->
					<div class="modal fade" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Score Détaillé - <?php echo $tab->nom; ?> - <?php echo $tab->datesave; ?></h4>
						  </div>
						  <div class="modal-body">
<?php
								echo '<table class="table" border=2>';
								echo 	'<tr>
											<td><h6>TROU</h6></td><td><h6>DISTANCE</h6></td><td><h6>PAR</h6></td><td><h6>SCORE</h6></td>
										</tr>';
									
								foreach($tab->les_scores_trous as $letrou)
								{
									echo 	'<tr>
												<td><h6>'.$letrou->numero.'</h6></td>
												<td><h6>'.$letrou->distance.'</h6></td>
												<td><h6>'.$letrou->par.'</h6></td>
												<td><h6>'.$letrou->score.'</h6></td>';
												
										echo '</tr>';
								}
								echo '</table>';
								
								echo '<ul class="nav nav-pills"><li><a href="index.php?cas=classement&action=chercher_depuis_histo&nom_code='.$tab->nom.'"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Classement Club</a></li></ul>';
?>								
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
						  </div>
						</div>
					  </div>
					</div>	

				</div>
				
<?php				
			}
		}
		
		unset($_SESSION["scores_joueur"]);
	}
?>

</div> 






