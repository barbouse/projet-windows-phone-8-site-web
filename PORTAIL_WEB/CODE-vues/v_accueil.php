

<div class="row">
	<div class="col-lg-8 col-lg-offset-2 text-center">
	
		<div id="carousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#carousel" data-slide-to="0" class="active"></li>
			<li data-target="#carousel" data-slide-to="1"></li>
			<li data-target="#carousel" data-slide-to="2"></li>
			<li data-target="#carousel" data-slide-to="3"></li>
			<li data-target="#carousel" data-slide-to="4"></li>
			<li data-target="#carousel" data-slide-to="5"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">			
			
			<div class="item active">
			  <img src="fichiers/galerie/1.jpg" class="fullwidth" alt="1">
			  <div class="carousel-caption">
				<p>Golf Paradise</p>
			  </div>
			</div>
			
			
			<div class="item">
			  <img src="fichiers/galerie/2.jpg" class="fullwidth" alt="2">
			  <div class="carousel-caption">
				<p>Golf Cup 2014</p>
			  </div>
			</div>
			
			<div class="item">
			  <img src="fichiers/galerie/3.jpg" class="fullwidth" alt="3">
			  <div class="carousel-caption">
				<p>Be Golf !</p>
			  </div>
			</div>
			
			<div class="item">
			  <img src="fichiers/galerie/4.jpg" class="fullwidth" alt="4">
			  <div class="carousel-caption">
				<p>Golf View</p>
			  </div>
			</div>
			
			<div class="item">
			  <img src="fichiers/galerie/5.jpg" class="fullwidth" alt="5">
			  <div class="carousel-caption">
				<p>Ultra Golf Spot</p>
			  </div>
			</div>
			
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		</div>
		
	</div>
</div>


<script>

$('.carousel').carousel({
interval: 3000
});

</script>




