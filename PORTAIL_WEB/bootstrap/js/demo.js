/*
 * Bootstrap Image Gallery JS Demo 3.0.0
 * https://github.com/blueimp/Bootstrap-Image-Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint unparam: true */
/*global window, document, blueimp, $ */

$(function () {
    'use strict';

    

    $('#borderless-checkbox').on('change', function () {
        var borderless = $(this).is(':checked');
        $('#blueimp-gallery').data('useBootstrapModal', !borderless);
        $('#blueimp-gallery').toggleClass('blueimp-gallery-controls', borderless);
    });

    $('#fullscreen-checkbox').on('change', function () {
        $('#blueimp-gallery').data('fullScreen', $(this).is(':checked'));
    });

    $('#image-gallery-button').on('click', function (event) {
        event.preventDefault();
        blueimp.Gallery($('#links a'), $('#blueimp-gallery').data());
    });

    $('#video-gallery-button').on('click', function (event) {
        event.preventDefault();
        blueimp.Gallery([
            
            {
                title: 'Deck Tech 1',
                href: 'http://www.youtube.com/watch?v=GY6DgUUKL7Y',
                type: 'text/html',
                youtube: 'GY6DgUUKL7Y',
                poster: 'http://img.youtube.com/vi/GY6DgUUKL7Y/0.jpg'
            },
			
			{
                title: 'Deck Tech 2',
                href: 'http://www.youtube.com/watch?v=77hT7oDdvtk',
                type: 'text/html',
                youtube: '77hT7oDdvtk',
                poster: 'http://img.youtube.com/vi/77hT7oDdvtk/0.jpg'
            },
			
			{
                title: 'Deck Tech 3',
                href: 'http://www.youtube.com/watch?v=XdZHdWQWHT8',
                type: 'text/html',
                youtube: 'XdZHdWQWHT8',
                poster: 'http://img.youtube.com/vi/XdZHdWQWHT8/0.jpg'
            },
			
			{
                title: 'Deck Tech 4',
                href: 'http://www.youtube.com/watch?v=jp1eq56_kLU',
                type: 'text/html',
                youtube: 'jp1eq56_kLU',
                poster: 'http://img.youtube.com/vi/jp1eq56_kLU/0.jpg'
            },
			
			{
                title: 'Deck Tech 5',
                href: 'http://www.youtube.com/watch?v=96Z2LZ4zBHw',
                type: 'text/html',
                youtube: '96Z2LZ4zBHw',
                poster: 'http://img.youtube.com/vi/96Z2LZ4zBHw/0.jpg'
            },
			
			{
                title: 'Deck Tech 6',
                href: 'http://www.youtube.com/watch?v=AGZot67Jcv0',
                type: 'text/html',
                youtube: 'AGZot67Jcv0',
                poster: 'http://img.youtube.com/vi/AGZot67Jcv0/0.jpg'
            },
			
			{
                title: 'Deck Tech 7',
                href: 'http://www.youtube.com/watch?v=MiP9y-DtPnE',
                type: 'text/html',
                youtube: 'MiP9y-DtPnE',
                poster: 'http://img.youtube.com/vi/MiP9y-DtPnE/0.jpg'
            },
			
			{
                title: 'Deck Tech 8',
                href: 'http://www.youtube.com/watch?v=5EN9rx3MMc4',
                type: 'text/html',
                youtube: '5EN9rx3MMc4',
                poster: 'http://img.youtube.com/vi/5EN9rx3MMc4/0.jpg'
            },
			
			{
                title: 'Deck Tech 9',
                href: 'http://www.youtube.com/watch?v=AAfJ16NHESE',
                type: 'text/html',
                youtube: 'AAfJ16NHESE',
                poster: 'http://img.youtube.com/vi/AAfJ16NHESE/0.jpg'
            },
			
			{
                title: 'Deck Tech 10',
                href: 'http://www.youtube.com/watch?v=2dZXhSqNM2U',
                type: 'text/html',
                youtube: '2dZXhSqNM2U',
                poster: 'http://img.youtube.com/vi/2dZXhSqNM2U/0.jpg'
            },
           
        ], $('#blueimp-gallery').data());
    });

});
