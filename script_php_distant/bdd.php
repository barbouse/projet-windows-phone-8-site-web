
<?php

class PdoProjetGMA
{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=projet_gma';   		
      	private static $user='root' ;    		
      	private static $mdp='' ;	
		private static $monPdo;
		private static $monPdoProjetGMA = null;

		
	private function __construct()
	{
    		PdoProjetGMA::$monPdo = new PDO(PdoProjetGMA::$serveur.';'.PdoProjetGMA::$bdd, PdoProjetGMA::$user, PdoProjetGMA::$mdp); 
			PdoProjetGMA::$monPdo->query("SET CHARACTER SET utf8");
	}
	
	public function _destruct()
	{
		PdoProjetGMA::$monPdo = null;
	}

	public  static function getPdoProjetGMA()
	{
		if(PdoProjetGMA::$monPdoProjetGMA == null)
		{
			PdoProjetGMA::$monPdoProjetGMA= new PdoProjetGMA();
		}
		return PdoProjetGMA::$monPdoProjetGMA;  
	}

	public function verif_co($login, $mdp)
	{
		$safemdp = md5($mdp);
		$sql = "SELECT COUNT(*) FROM joueurs WHERE login='".$login."' AND mdp='".$mdp."'";
		$resu = PdoProjetGMA::$monPdo->query($sql);
		$check = $resu->fetchColumn();
		if($check >= 1)
		{
			return "1";
		}
		else
			return "0";
	}
	
	public function sauvegarder_scores($login, $data)
	{
		date_default_timezone_set('Europe/Paris');
		
		// Récupération d'infos diverses servant pour la requête SQL :
		
			$datenow = date("Y-m-d");
			$code_club = $data->Code;
			
			
		// Récupération du MAX(scoregroup)   (on doit simplement incrémenter ce max de 1 pour faire nos insertions de trous)
		
			$scoregroup = PdoProjetGMA::$monPdo->query("SELECT MAX(scoregroup) AS max_score_group 
															FROM scoretrous");
			$scoregroup->setFetchMode(PDO::FETCH_OBJ);
			
			
			while( $ligne = $scoregroup->fetch() )
			{
				$max_score_group = $ligne->max_score_group;
			}
			
			// Le score_group à enregistrer
				$score_group = $max_score_group + 1;
		
			
		// Enregistrement des scores
		
			foreach($data->LesTrous as $letrou)
			{
				// Insertion du score pour le trou en cours
				
				PdoProjetGMA::$monPdo->query('INSERT INTO scoretrous (scoregroup, score, datesave, numero, code, login)
															VALUES(
																		"'.$score_group.'", 
																		"'.$letrou->Score.'",
																		"'.$datenow.'",
																		"'.$letrou->Numero.'", 
																		"'.$code_club.'",
																		"'.$login.'"
																	)
											');
			
			}
	}
	
	
	public function enregistrer_clubs($lesclubs)
	{
		foreach($lesclubs as $leclub)
		{
			$sql = 'SELECT COUNT(*) FROM clubs WHERE code="'.$leclub->Code.'"';
			$check = PdoProjetGMA::$monPdo->query($sql)->fetchColumn();
			
			if($check == 0)
			{
				// On insère le club dans la BDD
				
				PdoProjetGMA::$monPdo->query('INSERT INTO clubs (code, nom, rue, cp, ville, latitude, longitude, tel, email, licencies)
															VALUES(
																		"'.$leclub->Code.'", 
																		"'.$leclub->Nom.'",
																		"'.$leclub->Rue.'",
																		"'.$leclub->Cp.'", 
																		"'.$leclub->Ville.'",
																		"'.$leclub->Latitude.'",
																		"'.$leclub->Longitude.'",
																		"'.$leclub->Tel.'",
																		"'.$leclub->Email.'",
																		'.$leclub->Licencies.'
																	)
											');
				
				
				
				// On insère les trous de ce club dans la BDD
				
				foreach($leclub->LesTrous as $letrou)
				{
					PdoProjetGMA::$monPdo->query('INSERT INTO trous (numero, distance, par, code)
															VALUES(
																		'.$letrou->Numero.', 
																		'.$letrou->Distance.',
																		'.$letrou->Par.',
																		"'.$leclub->Code.'"
																	)
												');
				
				}		
				
			}		
		}
	
	}
	
	
	public function enregistrer_trous($lestrous, $code_club)
	{
		foreach($lestrous as $letrou)
		{
			$sql = 'SELECT COUNT(*) FROM trous WHERE code="'.$code_club.'" AND numero="'.$letrou->Numero.'"';
			$check = PdoProjetGMA::$monPdo->query($sql)->fetchColumn();
			
			if($check == 0)
			{
				PdoProjetGMA::$monPdo->query('INSERT INTO trous (numero, distance, par, code)
														VALUES(
																	"'.$letrou->Numero.'", 
																	"'.$letrou->Distance.'",
																	"'.$letrou->Par.'",
																	"'.$code_club.'"
																)
											');
			}		
		}
	}
	
} // FIN Classe PDO


?>











